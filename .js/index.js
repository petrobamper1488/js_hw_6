function createNewUser() {
  let userFirstName;
    do {
      userFirstName = prompt("Введіть Ваше ім'я:");
    } while (!userFirstName || !/^[A-Za-z]+$/.test(userFirstName));

  let userLastName;
    do {
      userLastName = prompt("Введіть Ваше прізвище:");
    } while (!userLastName || !/^[a-zA-Z\s]+$/.test(userLastName));
  
  let now = new Date;
  let userbirthday;
    do {
      userbirthday = prompt("Введіть дату Вашого народження в форматі dd.mm.yyyy")
    } while (!userbirthday || !/^(0[1-9]|1\d|2[0-9]|3[01])\.(0[1-9]|1[0-2])\.(19|20)\d{2}$/.test(userbirthday));

  let newUser = {
      _firstName: userFirstName,
      _lastName: userLastName,
      birthday: new Date(userbirthday.slice(6, 10), ((+userbirthday.slice(3, 5)) - 1), userbirthday.slice(0, 2)),
      set firstName (value) {
        if (typeof value === "string") {
        this._firstName = value;
        }
      },
      get firstName () {
        return this._firstName;
      },
      set lastName (value) {
        if (typeof value === "string") {
        this._lastName = value;
        }
      },
      get lastName () {
        return this._lastName;
      },
      getLogin: function () {
        return (this._firstName.charAt(0) + this._lastName).toLowerCase();
      },

      getAge: function () {
        return Math.floor((new Date().getTime() - new Date(this.birthday)) / ((24 * 3600 * 365.2 * 1000)));
      },

      getPassword: function () {
        return `${this._firstName[0].toUpperCase()}${this._lastName.toLowerCase()}${userbirthday.slice(6, 10)}`;
      }
  };
  return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());